# Ringkasan

* Pelajari dasar-dasar penyusunan aplikasi yang tangguh dengan [Panduan arsitektur aplikasi](https://developer.android.com/jetpack/docs/guide?authuser=1&hl=id).
* Kelola siklus proses aplikasi Anda. [Komponen berbasis siklus proses](https://developer.android.com/topic/libraries/architecture/lifecycle?authuser=1&hl=id) yang baru akan membantu mengelola aktivitas dan siklus proses fragmen Anda. Bertahan dari perubahan konfigurasi, hindari kebocoran memori, dan muat data ke UI Anda dengan mudah.
* Gunakan [LiveData](https://developer.android.com/topic/libraries/architecture/livedata?authuser=1&hl=id) untuk membuat objek data yang memberi notifikasi tampilan saat terjadi perubahan database yang mendasarinya.
* [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel?authuser=1&hl=id) menyimpan data terkait UI yang tidak dihapus saat rotasi aplikasi.
* [Room](https://developer.android.com/topic/libraries/architecture/room?authuser=1&hl=id) adalah library pemetaan objek SQLite. Gunakan untuk menghindari kode boilerplate dan mengonversi data table SQLite menjadi objek Java dengan mudah. Room akan menyediakan pemeriksaan waktu kompilasi terhadap pernyataan SQLite dan dapat menampilkan LiveData, Flowable, dan RxJava yang dapat diamati.

### Referensi lainnya <a id="additional-resources"></a>

Untuk mempelajari Komponen Arsitektur Android selengkapnya, lihat referensi berikut.

#### Contoh <a id="samples"></a>

* [Sunflower](https://github.com/android/sunflower), yakni aplikasi berkebun yang mengilustrasikan praktik terbaik pengembangan Android dengan Android Jetpack.
* [Contoh Browser GitHub Komponen Arsitektur Android](https://github.com/android/architecture-components-samples/tree/main/GithubBrowserSample)
* [\(lainnya...\)](https://developer.android.com/topic/libraries/architecture/additional-resources?authuser=1&hl=id#samples)

#### Codelab <a id="codelabs"></a>

* Android Room dengan View [\(Java\)](https://codelabs.developers.google.com/codelabs/android-room-with-a-view?authuser=1&hl=id) [\(Kotlin\)](https://codelabs.developers.google.com/codelabs/android-room-with-a-view-kotlin?authuser=1&hl=id)
* [Codelab Data Binding Android](https://codelabs.developers.google.com/codelabs/android-databinding?authuser=1&hl=id)
* [\(lainnya...\)](https://developer.android.com/topic/libraries/architecture/additional-resources?authuser=1&hl=id#codelabs)

#### Pelatihan <a id="training"></a>

* [Udacity: Mengembangkan Aplikasi Android dengan Kotlin](https://www.udacity.com/course/developing-android-apps-with-kotlin--ud9012)

#### Postingan blog <a id="blog-posts"></a>

* [Library Data Binding Android. Dari Kolom Observable Hingga LiveData dalam dua langkah](https://medium.com/androiddevelopers/android-data-binding-library-from-observable-fields-to-livedata-in-two-steps-690a384218f2)
* [Coroutine mudah di Android: viewModelScope](https://medium.com/androiddevelopers/easy-coroutines-in-android-viewmodelscope-25bffb605471)
* [\(lainnya...\)](https://developer.android.com/topic/libraries/architecture/additional-resources?authuser=1&hl=id#blogs)

#### Video <a id="videos"></a>

* [Yang Baru di Komponen Arsitektur \(Google I/O'19\)](https://www.youtube.com/watch?v=Qxj2eBmXLHg&authuser=1&hl=id)
* [Navigasi Jetpack \(Google I/O'19\)](https://www.youtube.com/watch?v=JFGq0asqSuA&authuser=1&hl=id)
* [\(lainnya...\)](https://developer.android.com/topic/libraries/architecture/additional-resources?authuser=1&hl=id#videos)

