---
description: Oleh Al-Ustadz Yazid bin ‘Abdul Qadir Jawas حفظه الله
---

# Menuntut Ilmu Jalan Menuju Surga

بِسْمِ اللّٰهِ الرَّحْمٰنِ الرَّحِيْمِ

 إِنَّ الْـحَمْدَ ِلِلهِ، نَحْمَدُهُ وَنَسْتَعِيْنُهُ وَنَسْتَغْفِرُهُ، وَنَعُوْذُ بِاللهِ مِنْ شُرُوْرِ أَنْفُسِنَا وَمِنْ سَيِّئَاتِ أَعْمَالِنَا، مَنْ يَهْدِهِ اللهُ فَلَا مُضِلَّ لَهُ، وَمَنْ يُضْلِلْ فَلَا هَادِيَ لَهُ، وَأَشْـهَدُ أَنْ لَا إِلَهَ إِلاَّ اللهُ وَحْدَهُ لَا شَرِيْكَ لَهُ، وَأَشْهَدُ أَنَّ مُحَمَّدًا عَبْدُهُ وَرَسُوْلُهُ.

_Segala puji hanya bagi Allah, kami memuji-Nya, memohon pertolongan dan ampunan kepada-Nya, kami berlindung kepada Allah dari kejahatan diri-diri kami dan kejelekan amal perbuatan kami. Barangsiapa yang Allah beri petunjuk, maka tidak ada yang dapat menyesatkannya, dan barangsiapa yang Allah sesatkan, maka tidak ada yang dapat memberinya petunjuk._ 

_Aku bersaksi bahwasanya tidak ada ilah yang berhak diibadahi dengan benar kecuali Allah semata, tiada sekutu bagi-Nya, dan aku bersaksi bahwasanya Nabi Muhammad shallallaahu ‘alaihi wa sallam adalah hamba dan Rasul-Nya._ 

يَا أَيُّهَا الَّذِينَ آمَنُوا اتَّقُوا اللَّهَ حَقَّ تُقَاتِهِ وَلَا تَمُوتُنَّ إِلَّا وَأَنْتُمْ مُسْلِمُونَ 

“Wahai orang-orang yang beriman! Bertakwalah kepada Allah dengan sebenar-benar takwa kepada-Nya dan janganlah kamu mati kecuali dalam keadaan muslim.” \[Ali ‘Imran/3:102\]

يَا أَيُّهَا النَّاسُ اتَّقُوا رَبَّكُمُ الَّذِي خَلَقَكُمْ مِنْ نَفْسٍ وَاحِدَةٍ وَخَلَقَ مِنْهَا زَوْجَهَا وَبَثَّ مِنْهُمَا رِجَالًا كَثِيرًا وَنِسَاءً ۚ وَاتَّقُوا اللَّهَ الَّذِي تَسَاءَلُونَ بِهِ وَالْأَرْحَامَ ۚ إِنَّ اللَّهَ كَانَ عَلَيْكُمْ رَقِيبً 

“Wahai manusia! Bertakwalah kepada Rabb-mu yang telah menciptakan kamu dari diri yang satu \(Adam\), dan \(Allah\) menciptakan pasangannya \(Hawa\) dari \(diri\)nya; dan dari keduanya Allah memperkembang-biakkan laki-laki dan perempuan yang banyak. Bertakwalah kepada Allah yang dengan Nama-Nya kamu saling meminta, dan \(peliharalah\) hubungan kekeluargaan. Sesungguhnya Allah selalu menjaga dan mengawasimu.” \[An-Nisaa’/4:1\]

يَا أَيُّهَا الَّذِينَ آمَنُوا اتَّقُوا اللَّهَ وَقُولُوا قَوْلًا سَدِيدً﴿٧٠﴾يُصْلِحْ لَكُمْ أَعْمَالَكُمْ وَيَغْفِرْ لَكُمْ ذُنُوبَكُمْ ۗ وَمَنْ يُطِعِ اللَّهَ وَرَسُولَهُ فَقَدْ فَازَ فَوْزًا عَظِيمًا

“Wahai orang-orang yang beriman! Bertakwalah kamu kepada Allah dan ucapkanlah perkataan yang benar, niscaya Allah akan memperbaiki amal-amalmu dan mengampuni dosa-dosamu. Dan barangsiapa mentaati Allah dan Rasul-Nya, maka sungguh ia menang dengan kemenangan yang besar.” \[Al-Ahzaab/33: 70-71\]

فَإِنَّ أَصْدَقَ الْـحَدِيْثِ كِتَابُ اللهِ وَخَيْرَ الْـهَدْيِ هَدْيُ مُحَمَّدٍ صَلَّى اللهُ عَلَيْهِ وَسَلَّمَ وَشَرَّ اْلأُمُوْرِ مُحْدَثَاتُهَا وَكُلَّ مُحْدَثَةٍ بِدْعَةٌ وَكُلَّ بِدْعَةٍ ضَلَالَةٌ وَكُلَّ ضَلَالَةٍ فِـي النَّارِ.

Sesungguhnya sebenar-benar perkataan adalah Kitabullah \(Al-Qur-an\) dan sebaik-baik petunjuk adalah petunjuk Muhammad shallallaahu ‘alaihi wa sallam \(As-Sunnah\). Seburuk-buruk perkara adalah perkara yang diada-adakan \(dalam agama\), setiap yang diada-adakan \(dalam agama\) adalah bid’ah, setiap bid’ah adalah sesat, dan setiap kesesatan tempatnya di Neraka. 

Amma ba’du: 

Kepada saudara-saudaraku seiman dan se’aqidah…

Selayaknyalah kita bersyukur kepada Allah Subhanahu wa Ta’ala atas segala nikmat yag Allah karuniakan kepada kita yang semua itu wajib untuk kita syukuri. Nikmat yang Allah berikan kepada kita sangatlah banyaki, tidak dapat dan tidak akan dapat kita hitung. Maka kewajiban seorang Muslim dan Muslimah adalah mensyukuri nikmat-nikmat yang Allah karuniakan kepada kita. Di antaranya adalah nikmat Islam, nikmat iman, nikmat sehat, nikmat rizki, dan lainnya yang Allah berikan kepada kita.

Mensyukuri nikmat-nikmat Allah adalah wajib hukumnya. Allah Subhaanahu wa Ta’aala berfirman:

 وَآتَاكُمْ مِنْ كُلِّ مَا سَأَلْتُمُوهُ ۚ وَإِنْ تَعُدُّوا نِعْمَتَ اللَّهِ لَا تُحْصُوهَا ۗ إِنَّ الْإِنْسَانَ لَظَلُومٌ كَفَّارٌ 

“Dan jika kamu menghitung nikmat-nikmat Allah, niscaya kamu tidak akan dapat menghitungnya. Sesungguhnya manusia sangat zhalim dan sangat mengingkari \(nikmat Allah\).” \[Ibrahim/14:34\]

Allah Subhanahu wa Ta’ala mengingatkan bahwa manusia sangat zhalim dan sangat kufur karena mereka tidak mensyukuri nikmat-nikmat Allah yang diberikan kepada mereka. 

Di antara nikmat yang Allah berikan kepada kita adalah nikmat Islam, iman, rizki, harta, umur, waktu luang, dan kesehatan untuk beribadah kepada Allah dengan benar dan untuk menuntut ilmu syar’i. 

Manusia diberikan dua kenikmatan, namun banyak di antara mereka yang tertipu. Rasulullah shallallaahu ‘alaihi wa sallam bersabda:

 نِعْمَتَانِ مَغْبُوْنٌ فِيْهِمَا كَثِيْرٌ مِنَ النَّاسِ الصِّحَّةُ وَالْفَرَاغُ. 

“Dua nikmat yang banyak manusia tertipu dengan keduanya, yaitu nikmat sehat dan waktu luang.”\[1\] 

Banyak di antara manusia yang tidak menggunakan waktu sehat dan waktu luangnya dengan sebaik-baiknya. Ia tidak gunakan untuk belajar tentang Islam, tidak ia gunakan untuk menimba ilmu syar’i. Padahal dengan menghadiri majelis taklim yang mengajarkan Al-Quran dan As-Sunnah menurut pemahaman para Shahabat, akan bertambah ilmu, keimanan, dan ketakwaannya kepada Allah Subhanahu wa Ta’ala. Juga dapat menambah amal kebaikannya. 

Semoga melalui majelis taklim yang kita kaji dari kitab-kitab para ulama Salaf, Allah memberikan hidayah kepada kita di atas Islam, ditetapkan hati dalam beriman, istiqamah di atas Sunnah, serta diberikan hidayah taufik oleh Allah untuk dapat melaksanakan syari’at Islam secara kaffah \(menyeluruh\) dan kontinyu hingga kita diwafatkan oleh Allah Subhanahu wa Ta’ala dalam keadaan mentauhidkan Allah dan melaksanakan Sunnah. Semoga Allah senantiasa memudahkan kita untuk selalu menuntut ilmu syar’i, diberikan kenikmatan atasnya, dan diberikan pemahaman yang benar tentang Islam dan Sunnah menurut pemahaman Salafush Shalih. 

Setiap Muslim dan Muslimah diperintahkan untuk menuntut ilmu karena dengan menuntut ilmu mereka akan mengetahui tentang agama Islam yang bersumber dari Al-Qur-an dan As-Sunnah. Seorang Muslim tidak akan bisa melaksanakan agamanya dengan benar, kecuali dengan belajar Islam yang benar berdasarkan Al-Qur-an dan As-Sunnah menurut pemahaman Salafush Shalih. Agama Islam adalah agama ilmu dan amal karena Nabi shallallaahu ‘alaihi wa sallam diutus dengan membawa ilmu dan amal shalih. 

Allah Subhanahu wa Ta’ala berfirman:

 هُوَ الَّذِي أَرْسَلَ رَسُولَهُ بِالْهُدَىٰ وَدِينِ الْحَقِّ لِيُظْهِرَهُ عَلَى الدِّينِ كُلِّهِ ۚ وَكَفَىٰ بِاللَّهِ شَهِيدًا 

“Dia-lah yang mengutus Rasul-Nya dengan membawa petunjuk dan agama yang hak agar dimenangkan-Nya terhadap semua agama. Dan cukuplah Allah sebagai saksi.” \[Al-Fat-h/48:28\] 

Yang dimaksud dengan al-hudaa \(petunjuk\) dalam ayat ini adalah ilmu yang bermanfaat. Dan yang dimaksud dengan diinul haqq \(agama yang benar\) adalah amal shalih. Allah Ta’ala mengutus Nabi Muhammad shallallahu ‘alaihi wa sallam untuk menjelaskan kebenaran dari kebatilan, menjelaskan Nama-Nama Allah, sifat-sifat-Nya, perbuatan-perbuatan-Nya, hukum-hukum dan berita yang datang dari-Nya, serta memerintahkan untuk melakukan segala apa yang bermanfaat bagi hati, ruh, dan jasad. 

Beliau shallallaahu ‘alaihi wa sallam menyuruh ummat-nya agar mengikhlaskan ibadah semata-mata karena Allah Ta’ala, mencintai-Nya, berakhlak yang mulia, beradab dengan adab yang baik dan melakukan amal shalih. Beliau shallallaahu ‘alaihi wa sallam melarang ummatnya dari perbuatan syirik, amal dan akhlak yang buruk, yang berbahaya bagi hati, badan, dan kehidupan dunia dan akhiratnya.\[2\] 

          __**Baca Juga**[ **Kaidah-Kaidah Menuntut Ilmu** ](https://almanhaj.or.id/2764-kaidah-kaidah-menuntut-ilmu.html)\*\*\*\*

Cara untuk mendapat hidayah dan mensyukuri nikmat Allah adalah dengan menuntut ilmu syar’i. Menuntut ilmu adalah jalan yang lurus untuk dapat membedakan antara yang haq dan yang bathil, Tauhid dan syirik, Sunnah dan bid’ah, yang ma’ruf dan yang munkar, dan antara yang bermanfaat dan yang membahayakan. Menuntut ilmu akan menambah hidayah serta membawa kepada kebahagiaan dunia dan akhirat. 

Seorang Muslim tidaklah cukup hanya dengan menyatakan keislamannya tanpa berusaha untuk memahami Islam dan mengamalkannya. Pernyataannya harus dibuktikan dengan melaksanakan konsekuensi dari Islam. Karena itulah menuntut ilmu merupakan jalan menuju kebahagiaan yang abadi. 

### 1. Menuntut Ilmu Syar’i Wajib bagi Setiap Muslim dan Muslimah 

Rasulullah shallallaahu ‘alaihi wa sallam bersabda,

 طَلَبُ الْعِلْمِ فَرِيْضَةٌ عَلَى كُلِّ مُسْلِمٍ. 

“Menuntut ilmu itu wajib atas setiap Muslim.”\[3\]

 Imam al-Qurthubi rahimahullaah menjelaskan bahwa hukum menuntut ilmu terbagi dua: 

**Pertama**, hukumnya wajib ; seperti menuntut ilmu tentang shalat, zakat, dan puasa. Inilah yang dimaksudkan dalam riwayat yang menyatakan bahwa menuntut ilmu itu \(hukumnya\) wajib. 

**Kedua**, hukumnya fardhu kifayah ; seperti menuntut ilmu tentang pembagian berbagai hak, tentang pelak-sanaan hukum hadd \(qishas, cambuk, potong tangan dan lainnya\), cara mendamaikan orang yang bersengketa, dan semisalnya. Sebab, tidak mungkin semua orang dapat mempelajarinya dan apabila diwajibkan bagi setiap orang tidak akan mungkin semua orang bisa melakukannya, atau bahkan mungkin dapat meng-hambat jalan hidup mereka. Karenanya, hanya beberapa orang tertentu sajalah yang diberikan kemudahan oleh Allah dengan rahmat dan hikmah-Nya. 

Ketahuilah, menuntut ilmu adalah suatu kemuliaan yang sangat besar dan menempati kedudukan tinggi yang tidak sebanding dengan amal apa pun.\[4\] 

### 2. Menuntut Ilmu Syar’i Memudahkan Jalan Menuju Surga 

Setiap Muslim dan Muslimah ingin masuk Surga. Maka, jalan untuk masuk Surga adalah dengan menuntut ilmu syar’i. Sebab Rasulullah shallallaahu ‘alaihi wa sallam bersabda,

 مَنْ نَفَّسَ عَنْ مُؤْمِنٍ كُرْبَةً مِنْ كُرَبِ الدُّنْيَا، نَفَّسَ اللهُ عَنْهُ كُرْبَةً مِنْ كُرَبِ يَوْمِ الْقِيَامَةِ، وَمَنْ يَسَّرَ عَلَى مُعْسِرٍ، يَسَّرَ اللهُ عَلَيْهِ فِي الدُّنْيَا وَاْلآخِرَةِ، وَمَنْ سَتَرَ مُسْلِمًا، سَتَرَهُ اللهُ فِي الدُّنْيَا وَاْلآخِرَةِ، وَاللهُ فِي عَوْنِ الْعَبْدِ مَا كَانَ الْعَبْدُ فِي عَوْنِ أَخِيهِ، وَمَنْ سَلَكَ طَرِيقًا يَلْتَمِسُ فِيهِ عِلْمًا، سَهَّلَ اللهُ لَهُ بِهِ طَرِيقًا إِلَى الْجَنَّةِ، وَمَا اجْتَمَعَ قَوْمٌ فِي بَيْتٍ مِنْ بُيُوتِ اللهِ يَتْلُونَ كِتَابَ اللهِ وَيَتَدَارَسُونَهُ بَيْنَهُمْ، إِلَّا نَزَلَتْ عَلَيْهِمُ السَّكِينَةُ، وَغَشِيَتْهُمُ الرَّحْمَةُ، وَحَفَّتْهُمُ الْـمَلاَئِكَةُ، وَذَكَرَهُمُ اللهُ فِيمَنْ عِنْدَهُ، وَمَنْ بَطَّأَ بِهِ عَمَلُهُ، لَـمْ يُسْرِعْ بِهِ نَسَبُهُ. 

“Barangsiapa yang melapangkan satu kesusahan dunia dari seorang mukmin, maka Allah melapangkan darinya satu kesusahan di hari Kiamat. Barangsiapa memudahkan \(urusan\) atas orang yang kesulitan \(dalam masalah hutang\), maka Allah memudahkan atasnya di dunia dan akhirat. Barangsiapa menutupi \(aib\) seorang muslim, maka Allah menutupi \(aib\)nya di dunia dan akhirat. Allah senantiasa menolong hamba selama hamba tersebut senantiasa menolong saudaranya. **Barangsiapa yang meniti suatu jalan untuk mencari ilmu, maka Allah memudahkan untuknya jalan menuju Surga**. Tidaklah suatu kaum berkumpul di salah satu rumah Allah \(masjid\) untuk membaca Kitabullah dan mempelajarinya di antara mereka, melainkan ketenteraman turun atas mereka, rahmat meliputi mereka, Malaikat mengelilingi mereka, dan Allah menyanjung mereka di tengah para Malaikat yang berada di sisi-Nya. Barangsiapa yang lambat amalnya, maka tidak dapat dikejar dengan nasabnya.”\[5\] 

Di dalam hadits ini terdapat janji Allah ‘Azza wa Jalla bahwa bagi **orang-orang yang berjalan dalam rangka menuntut ilmu syar’i, maka Allah akan memudahkan jalan baginya menuju Surga**. 

**“Berjalan menuntut ilmu”** mempunyai dua makna. **Pertama**, menempuh jalan dengan artian yang sebenarnya, yaitu berjalan kaki menuju majelis-majelis para ulama. **Kedua**, menempuh jalan \(cara\) yang mengantarkan seseorang untuk mendapatkan ilmu seperti menghafal, belajar \(sungguh-sungguh\), membaca, menela’ah kitab-kitab \(para ulama\), menulis, dan berusaha untuk memahami \(apa-apa yang dipelajari\). Dan cara-cara lain yang dapat mengantarkan seseorang untuk mendapatkan ilmu syar’i. 

**“Allah akan memudahkan jalannya menuju Surga”** mempunyai dua makna. **Pertama**, Allah akan memudahkan memasuki Surga bagi orang yang menuntut ilmu yang tujuannya untuk mencari wajah Allah, untuk mendapatkan ilmu, mengambil manfaat dari ilmu syar’i dan mengamalkan konsekuensinya. **Kedua**, Allah akan memudahkan baginya jalan ke Surga pada hari Kiamat ketika melewati “shirath” dan dimudahkan dari berbagai ketakutan yang ada sebelum dan sesudahnya. Wallaahu a’lam.¨∗∗ 

Juga dalam sebuah hadits panjang yang berkaitan tentang ilmu, Rasulullah shallallaahu ‘alaihi wa sallam bersabda,

 مَنْ سَلَكَ طَرِيْقًا يَطْلُبُ فِيْهِ عِلْمًا سَلَكَ اللهُ بِهِ طَرِيْقًا إِلَى الْـجَنَّةِ وَإِنَّ الْـمَلاَئِكَةَ لَتَضَعُ أَجْنِحَتَهَا رِضًا لِطَالِبِ الْعِلْمِ وَإِنَّهُ لَيَسْتَغْفِرُ لِلْعَالِـمِ مَنْ فِى السَّمَاءِ وَاْلأَرْضِ حَتَّى الْـحِيْتَانُ فِى الْـمَاءِ وَفَضْلُ الْعَالِـمِ عَلَى الْعَابِدِ كَفَضْلِ الْقَمَرِ عَلَى سَائِرِ الْكَوَاكِبِ. إِنَّ الْعُلَمَاءَ هُمْ وَرَثَةُ اْلأَنْبِيَاءِ لَـمْ يَرِثُوا دِيْنَارًا وَلاَ دِرْهَمًا وَإِنَّمَا وَرَثُوا الْعِلْمَ فَمَنْ أَخَذَهُ أَخَذَ بِحَظٍّ وَافِرٍ. 

“**Barangsiapa yang berjalan menuntut ilmu, maka Allah mudahkan jalannya menuju Surga**. Sesungguhnya Malaikat akan meletakkan sayapnya untuk orang yang menuntut ilmu karena ridha dengan apa yang mereka lakukan. Dan sesungguhnya seorang yang mengajarkan kebaikan akan dimohonkan ampun oleh makhluk yang ada di langit maupun di bumi hingga ikan yang berada di air. Sesungguhnya keutamaan orang ‘alim atas ahli ibadah seperti keutamaan bulan atas seluruh bintang. Sesungguhnya para ulama itu pewaris para Nabi. Dan sesungguhnya para Nabi tidak mewariskan dinar tidak juga dirham, yang mereka wariskan hanyalah ilmu. Dan barangsiapa yang mengambil ilmu itu, maka sungguh, ia telah mendapatkan bagian yang paling banyak.”\[6\] 

Jika kita melihat para Shahabat radhiyallaahu anhum ajma’in, mereka bersungguh-sungguh dalam menuntut ilmu syar’i. Bahkan para Shahabat wanita juga bersemangat menuntut ilmu. Mereka berkumpul di suatu tempat, lalu Nabi shallallaahu ‘alaihi wa sallam mendatangi mereka untuk menjelaskan tentang Al-Qur-an, menjelaskan pula tentang Sunnah-Sunnah Nabi shallallaahu ‘alaihi wa sallam. Allah Ta’ala juga memerintahkan kepada wanita untuk belajar Al-Qur-an dan As-Sunnah di rumah mereka. 

Sebagaimana yang Allah Ta’ala firmankan,

 وَقَرْنَ فِي بُيُوتِكُنَّ وَلَا تَبَرَّجْنَ تَبَرُّجَ الْجَاهِلِيَّةِ الْأُولَىٰ ۖ وَأَقِمْنَ الصَّلَاةَ وَآتِينَ الزَّكَاةَ وَأَطِعْنَ اللَّهَ وَرَسُولَهُ ۚ إِنَّمَا يُرِيدُ اللَّهُ لِيُذْهِبَ عَنْكُمُ الرِّجْسَ أَهْلَ الْبَيْتِ وَيُطَهِّرَكُمْ تَطْهِيرًا﴿ ٣٣﴾ وَاذْكُرْنَ مَا يُتْلَىٰ فِي بُيُوتِكُنَّ مِنْ آيَاتِ اللَّهِ وَالْحِكْمَةِ ۚ إِنَّ اللَّهَ كَانَ لَطِيفًا خَبِيرًا

**Baca Juga** [Akhlak Penuntut Ilmu : Menerima Hukum-Hukum Allah Dengan Bentuk Mengamalkannya](https://almanhaj.or.id/1200-akhlak-penuntut-ilmu-menerima-hukum-hukum-allah-dengan-bentuk-mengamalkannya.html)

“Dan hendaklah kamu tetap di rumahmu dan janganlah kamu berhias dan \(bertingkah laku\) seperti orang-orang Jahiliyyah dahulu, dan laksanakanlah shalat, tunaikanlah zakat, taatilah Allah dan Rasul-Nya. Sesungguhnya Allah bermaksud hendak menghilangkan dosa dari kamu, wahai Ahlul Bait, dan membersihkan kamu dengan sebersih-bersihnya. Dan ingatlah apa yang dibacakan di rumahmu dari ayat-ayat Allah dan al-Hikmah \(Sunnah Nabimu\). Sungguh, Allah Mahalembut, Maha Mengetahui.” \[Al-Ahzaab/33: 33-34\]

Laki-laki dan wanita diwajibkan menuntut ilmu, yaitu ilmu yang bersumber dari Al-Qur-an dan As-Sunnah karena dengan ilmu yang dipelajari, ia akan dapat mengerjakan amal-amal shalih, yang dengan itu akan mengantarkan mereka ke Surga.

Kewajiban menuntut ilmu ini mencakup seluruh individu Muslim dan Muslimah, baik dia sebagai orang tua, anak, karyawan, dosen, Doktor, Profesor, dan yang lainnya. Yaitu mereka wajib mengetahui ilmu yang berkaitan dengan muamalah mereka dengan Rabb-nya, baik tentang Tauhid, rukun Islam, rukun Iman, akhlak, adab, dan mu’amalah dengan makhluk. 

### 3. Majelis-Majelis Ilmu adalah Taman-Taman Surga 

_Nabi shallallaahu ‘alaihi wa sallam bersabda_,

 إِذَا مَرَرْتُمْ بِرِيَاضِ الْـجَنَّةِ فَارْتَعُوْا، قَالُوْا: يَا رَسُوْلَ اللهِ مَا رِيَاضُ الْـجَنَّةِ؟ قَالَ: حِلَقُ الذِّكْرِ

“_Apabila kalian berjalan melewati taman-taman Surga, perbanyaklah berdzikir.” Para Shahabat bertanya, “Wahai Rasulullah, apakah yang dimaksud taman-taman Surga itu?” Beliau menjawab, “Yaitu halaqah-halaqah dzikir \(majelis ilmu\)_.”\[7\]

‘Atha’ bin Abi Rabah \(wafat th. 114 H\) rahimahullaah berkata, “_Majelis-majelis dzikir yang dimaksud adalah majelis-majelis halal dan haram, bagaimana harus membeli, menjual, berpuasa, mengerjakan shalat, menikah, cerai, melakukan haji, dan yang sepertinya_.”\[8\] 

Ketahuilah bahwa majelis dzikir yang dimaksud adalah majelis ilmu, majelis yang di dalamnya diajarkan tentang tauhid, ‘aqidah yang benar menurut pemahaman Salafush Shalih, ibadah yang sesuai Sunnah Nabi shal-lallaahu ‘alaihi wa sallam, muamalah, dan lainnya. 

Buku yang ada di hadapan pembaca merupakan buku **“Panduan Menuntut Ilmu”.** Di antara yang penulis jelaskan di dalamnya adalah keutamaan menuntut ilmu, kiat-kiat dalam meraih ilmu syar’i, penghalang-penghalang dalam memperoleh ilmu, adab-adab dalam menuntut ilmu, hal-hal yang harus dijauhkan oleh para penuntut ilmu, perjalanan ulama dalam menuntut ilmu, dan yang lainnya. Penulis jelaskan masalah menuntut ilmu karena masalah ini sangatlah penting. Sebab, seseorang dapat memperoleh petunjuk, dapat memahami dan mengamalkan Islam dengan benar apabila ia belajar dari guru, kitab, dan cara yang benar. Sebaliknya, jika seseorang tidak mau belajar, atau ia belajar dari guru yang tidak mengikuti Sunnah, atau melalui cara belajar dan kitab yang dibacakan tidak benar, maka ia akan menyimpang dari jalan yang benar. 

Para ulama terdahulu telah menulis kitab-kitab panduan dalam menuntut ilmu, seperti Imam Ibnu ‘Abdil Barr dengan kitabnya _Jaami’ Bayaanil ‘Ilmi wa Fadhlihi_, Imam Ibnu Jama’ah dengan kitabnya _Tadzkiratus Samii’_, begitu pula al-Khatib al-Baghdadi yang telah menulis banyak sekali kitab tentang berbagai macam disiplin ilmu, bahkan pada setiap disiplin ilmu hadits beliau tulis dalam kitab tersendiri. Juga ulama selainnya seperti Imam Ibnul Jauzi, Syaikhul Islam Ibnu Taimiyyah \(dalam _Majmuu’ Fataawaa_-nya dan kitab-kitab lainnya\), Imam Ibnu Qayyim al-Jauziyyah \(dalam kitabnya _Miftaah Daaris Sa’aadah_ dan kitab-kitab lainnya\), dan masih banyak lagi para ulama lainnya hingga zaman sekarang ini, seperti Syaikh bin Baaz, Syaikh al-Albani, dan Syaikh al-‘Utsaimin _rahimahumullaah_.

Dalam buku ini, penulis berusaha menyusunnya dari berbagai kitab para ulama terdahulu hingga sekarang dengan harapan buku ini menjadi panduan agar memudahkan kaum Muslimin untuk menuntut ilmu, memberikan semangat dalam menuntut ilmu, beradab dan berakhlak serta berperangai mulia yang seharusnya dimiliki oleh setiap penuntut ilmu. Mudah-mudahan buku ini bermanfaat bagi penulis dan para pembaca sekalian, serta bagi kaum Muslimin. Mudah-mudahan amal ini diterima oleh Allah _Subhaanahu wa Ta’ala_ dan menjadi timbangan amal kebaikan penulis pada hari Kiamat. Dan mudah-mudahan dengan kita menuntut ilmu syar’i dan mengamalkannya, Allah _‘Azza wa Jalla_ akan memudahkan jalan kita untuk memasuki Surga-Nya. _Aamiin_.

Semoga shalawat dan salam senantiasa dilimpahkan kepada Rasulullah _shallallaahu ‘alaihi wa sallam_, keluarga dan para Shahabat beliau, serta orang-orang yang mengikuti jejak mereka dengan kebaikan hingga hari Kiamat.

Bogor, Sabtu 27 Shafar 1428 H  
17 Maret 2007 M

\[Disalin dari Muqaddimah buku Menuntut Ilmu Jalan Menuju Surga “Panduan Menuntut Ilmu”, Penulis Yazid bin Abdul Qadir Jawas, Penerbit Pustaka At-Taqwa, PO BOX 264 – Bogor 16001 Jawa Barat – Indonesia, Cetakan Pertama Rabi’uts Tsani 1428H/April 2007M\]   
_**\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_**_   
Footnote    
\[1\] Hadits shahih: Diriwayatkan oleh al-Bukhari \(no. 6412\), at-Tirmidzi \(no. 2304\), Ibnu Majah \(no. 4170\), Ahmad \(I/258,344\), ad-Darimi \(II/297\), al-Hakim \(IV/306\), dan selainnya dari Shahabat Ibnu ‘Abbas radhiyallaahu ‘anhuma.   
\[2\] Lihat kitab Taisiir Karimir Rahmaan fii Tafsiir Kalaamil Mannaan \(hal. 295-296\) karya Syaikh ‘Abdurrahman bin Nashir as-Sa’di \(wafat th. 1376 H\) rahimahullaah, cet. Mu-assasah ar-Risalah, th. 1417 H.   
\[3\] Hadits shahih: Diriwayatkan oleh Ibnu Majah \(no. 224\), dari Shahabat Anas bin Malik radhiyallahu ‘anhu, lihat Shahiih al-Jaami’ish Shaghiir \(no. 3913\). Diriwayatkan pula oleh Imam-imam ahli hadits yang lainnya dari beberapa Shahabat seperti ‘Ali, Ibnu ‘Abbas, Ibnu ‘Umar, Ibnu Mas’ud, Abu Sa’id al-Khudri, dan al-Husain bin ‘Ali radhiyallaahu ‘anhum.   
\[4\] Lihat Tafsiir al-Qurthubi \(VIII/187\), dengan diringkas. Tentang pembagian hukum menuntut ilmu dapat juga dilihat dalam Jaami’ Bayaanil ‘Ilmi wa Fadhlihi \(I/56-62\) oleh Ibnu ‘Abdil Barr.   
\[5\] Hadits shahih: Diriwayatkan oleh Muslim \(no. 2699\), Ahmad \(II/252, 325\), Abu Dawud \(no. 3643\), At-Tirmidzi \(no. 2646\), Ibnu Majah \(no. 225\), dan Ibnu Hibban \(no. 78-Mawaarid\), dari Shahabat Abu Hurairah radhiyallaahu ‘anhu. Lafazh ini milik Muslim. ¨∗∗ Jaami’ul ‘Uluum wal Hikam \(II/297\) dan Qawaa’id wa Fawaa-id minal Arba’iin an-Nawawiyyah \(hal. 316-317\).   
\[6\] Hadits shahih: Diriwayatkan oleh Ahmad \(V/196\), Abu Dawud \(no. 3641\), at-Tirmidzi \(no. 2682\), Ibnu Majah \(no. 223\), dan Ibnu Hibban \(no. 80—al-Mawaarid\), lafazh ini milik Ahmad, dari Shahabat Abu Darda’ radhiyallaahu ‘anhu.   
\[7\] Hadits hasan: Diriwayatkan oleh at-Tirmidzi \(no. 3510\), Ahmad \(III/150\) dan lainnya, dari Shahabat Anas bin Malik radhiyallaahu ‘anhu. At-Tirmidzi berkata, “Hadits ini hasan.” Lihat takhrij lengkapnya dalam Silsilah ash-Shahiihah \(no. 2562\).   
\[8\] Disebutkan oleh al-Khatib al-Baghdadi dalam al-Faqiih wal Mutafaqqih \(no. 40\). Lihat kitab al-‘Ilmu Fadhluhu wa Syarafuhu \(hal. 132\).

**Sumenep, Kamis 12 Sha'ban 1442 H  
25 Maret 2021**

Dikutip ulang tanpa perubahan dari   
[https://almanhaj.or.id/13056-menuntut-ilmu-jalan-menuju-surga-2.html](https://almanhaj.or.id/13056-menuntut-ilmu-jalan-menuju-surga-2.html)



وَمَنْ سَلَكَ طَرِيقًا يَلْتَمِسُ فِيهِ عِلْمًا سَهَّلَ اللَّهُ لَهُ بِهِ طَرِيقًا إِلَى الْجَنَّةِ

